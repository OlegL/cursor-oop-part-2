require_relative 'movable'

module HumanBeing
	GENDERS = ['male', 'female']

	class Person
		include Movable
		extend Movable

	NAMES = ['John', 'Erik', 'Roger']
	@@population = 0

	attr_accessor :name
	attr_reader :weight

	def initialize(name=nil)
		@name = name || Person.generate_name
		@birthday = DateTime.now
		@weight = 1

		@@population += 1

		puts "I was born! #{ @name }"
	end

	def self.population
		@@population
	end

	def self.generate_name
		NAMES.sample
	end

	def rename(new_name)
		@name = new_name
	end

	def birthday
		@birthday.strftime
	end

	def age
		@birthday - DateTime.now
	end

	def eat(food)
		accumulate_fat(food.to_f/fat_accumulation_speed)
	end

	protected

	def fat_accumulation_speed
		rand(7..50)
	end

	private

	# attr_writer :weight

	def accumulate_fat(food)
		@weight += food
	end
end
end

