require 'date'
require_relative 'person'
require_relative 'student'
require_relative 'movable'


puts HumanBeing::GENDERS
puts Movable.max_speed


john = HumanBeing::Person.new
ivan = Student.new

puts "Max speed #{ HumanBeing::Person.max_speed }"
john.move

# john.fat_accumulation_speed
# john.accumulate_fat(55)

puts "Before eating"
puts  john.weight
puts  ivan.weight

john.eat(4)
ivan.eat(4)

john.eat(15)
ivan.eat(15)

john.eat(1)
ivan.eat(1)

puts "After eating"
puts  john.weight
puts  ivan.weight